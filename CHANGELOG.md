
# CHANGELOG

## [0.1.3] - 2021-11-04

### Fixed

- editors with multiple persName or no persName at all are handled

### Added

- author emails are parsed. there was a slot in the object schema but it wasn't
  being populated


## [0.1.2] - 2021-10-28

### Added

- explicit `parse_citation_xml()` and `parse_citation_list_xml()` methods,
  which return Optional or always return a list. `parse_citations_xml()` is
  retained as an alias of `parse_citation_list_xml()`

## [0.1.1] - 2021-10-27

### Fixed

- `setup.py` fixes
- add `py.typed` file to pypi package so that mypy will detect annotations

## [0.1.0] - 2021-10-25

Initial public release.
