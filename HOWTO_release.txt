
Steps to cut a new release.

Final prep:

- `make fmt lint test`
- commit any last changes or cleanups
- update CHANGELOG with a version entry
- increment version number in `grobid_tei_xml/__init__.py`
- re-run tests, commit
- `git status`: ensure working directory is clean

tag, push, upload:

- `git tag vX.Y.Z -a -s -u bnewbold@archive.org -m "grobid_tei_xml release X.Y.Z"`
- `git push`, `git push --tags`
- `python3 setup.py upload` (ensure `bnewbold-archive` account)
