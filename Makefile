
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c
TODAY ?= $(shell date --iso --utc)

.PHONY: help
help: ## Print info about all commands
	@echo "Commands:"
	@echo
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "    \033[01;32m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: deps
deps: ## Install dependencies using pipenv
	pipenv install --dev

.PHONY: lint
lint: ## Run lints (eg, flake8, mypy)
	@pipenv run flake8 grobid_tei_xml/ tests/
	@pipenv run isort -q -c grobid_tei_xml/ tests/ || true
	@pipenv run mypy grobid_tei_xml/ tests/

.PHONY: fmt
fmt: ## Run code formatting on all source code
	pipenv run isort --atomic grobid_tei_xml/ tests/
	pipenv run black --line-length 96 grobid_tei_xml/ tests/

.PHONY: test
test: ## Run all tests and lints
	@pipenv run python -m pytest -vv

.PHONY: test-readme
test-readme: ## Test codeblocks in the README (includes live web requests)
	@pipenv run python -m pytest --codeblocks

.PHONY: coverage
coverage: ## Run all tests with coverage
	@pipenv run python -m pytest --cov --cov-report=term --cov-report=html
