from grobid_tei_xml import parse_document_xml


def test_small_xml_csl() -> None:

    with open("tests/files/small.xml", "r") as f:
        tei_xml = f.read()

    d = parse_document_xml(tei_xml)
    assert d.to_csl_dict() == {
        "type": "article-journal",
        "title": "Dummy Example File",
        "author": [
            {"given": "Brewster", "family": "Kahle"},
            {
                "given": "J",
                "family": "Doe",
            },
        ],
        "book-title": "Dummy Example File. Journal of Fake News. pp. 1-2. ISSN 1234-5678",
        "issued": [[2000]],
    }

    assert d.citations
    assert d.citations[0].to_csl_dict() == {
        "type": "article-journal",
        "title": "Everything is Wonderful",
        "author": [
            {"given": "A", "family": "Seaperson"},
        ],
        "container-title": "Letters in the Alphabet",
        "issued": [[2001]],
        "volume": 20,
        "page": "1-11",
        "page-first": "1",
    }
