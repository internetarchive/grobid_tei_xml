import xml.etree.ElementTree

import pytest

from grobid_tei_xml import parse_citation_list_xml


def test_doc_parse_error() -> None:
    """
    This XML document has a bare '&' (should be '&amp;') and results in a parse
    error.

    See also: https://github.com/kermitt2/grobid/issues/848

    The intent of this test is to ensure that the exception raised is the one
    expected, especially if that behavior changes in the future.
    """

    with open("tests/files/citation_list/parse_error.tei.xml", "r") as f:
        tei_xml = f.read()

    with pytest.raises(xml.etree.ElementTree.ParseError):
        parse_citation_list_xml(tei_xml)
